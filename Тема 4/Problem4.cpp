#include <iostream>
#include <string>
using namespace std;

int main()
{
string first;
string middle;
string last;
char sex;

cout<< "Enter your first, middle, last name and sex:" << endl;
cin>> first >> middle >> last >> sex;
string initials = first.substr(0,1) + middle.substr(0,1) + last.substr(0,1);

if (sex == 'M')
{
    cout<< "Hello Mr." << " " << last << "," << "your initials are:" << initials << endl;
}

else if (sex == 'F')
{
    cout<< "Hello Mrs." << " " << last << "," << "your initials are:" << initials << endl;
}

else
{
    cout<< "Error!" << endl;
}

return 0;
}


#include <iostream>
#include <limits>
using namespace std;

int main()
{
    int broqch;
    int min = std::numeric_limits<int>::max();
    int max = std::numeric_limits<int>::min();

    cout<< "Kolko chisla? " << endl;
    cin>> broqch;

    cout<< "Vavedi cislata: " << endl;

    for (int i=0; i<broqch; i++)
    {
        double nomer;
        cin>> nomer;

        if (nomer < min)
        {
            min = nomer;
        }

        else if (nomer > max)
        {
            max = nomer;
        }

    }

    cout<< "Nai-malkoto chislo e: " << min << endl;
    cout<< "Nai-golqmoto chislo e: " << max << endl;

    return 0;
}

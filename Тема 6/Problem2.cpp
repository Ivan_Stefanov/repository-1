#include <iostream>
#include <limits>
using namespace std;

int main()
{
    int smallest = numeric_limits<int>::max();
    int largest = 0;
    int cnt;
    cin>> cnt;

    for (int i=0; i<cnt; i++)
    {
        int num;
        cin>> num;

        if (num < smallest)
        {
            smallest = num;
        }

        else if (num > largest)
        {
            largest = num;
        }
    }

    cout<< "The smallest: " << smallest << endl;
    cout<< "The largest: " << largest << endl;

    return 0;
}

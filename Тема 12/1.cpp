#include <iostream>
#include <iomanip>
#include <cctype>
#include <vector>

using namespace std;

double BGN_USD = 0;
double BGN_EUR = 0;
double USD_EUR = 0;
double USD_BGN = 0;
double EUR_BGN = 0;
double EUR_USD = 0;


const int EXIT = -1;

//1) Complete the program to handle all currencies correctly.
//2) Find a way to improve the code reuse by using a function
//in convert and rate which will extract the exchange rate for
//a given pair of c1 and c2.

int exec_set()
{
    string c1, c2;
    double rate;
    cin >> c1 >> c2 >> rate;

    if(c1 == "BGN" && c2 == "USD")
        BGN_USD = rate;
    else if(c1 == "USD"&& c2 == "BGN")
        BGN_USD = 1/rate;
    else if(c1 == "USD" && c2 == "EUR")
        USD_EUR = rate;
    else if(c1 == "EUR" && c2 == "USD")
        EUR_USD = 1/rate;
    else if(c1 == "EUR" && c2 == "BGN")
        EUR_BGN = 1/rate;
    else if(c1 == "BGN" && c2 == "EUR")
        BGN_EUR = rate;

    return 0;
}

int exec_convert()
{
    cout << fixed << setprecision(2);

    string c1, c2;
    double value;
    cin >> c1 >> c2 >> value;

    double result;
    if(c1 == "BGN" && c2 == "USD")
        result = value * BGN_USD;
    else if(c1 == "USD"&& c2 == "BGN")
         result = value * (1/BGN_USD);
    else if(c1 == "USD" && c2 == "EUR")
        result = value * USD_EUR;
    else if(c1 == "EUR" && c2 == "USD")
        EUR_USD = value * (1/USD_EUR);
    else if(c1 == "EUR" && c2 == "BGN")
        EUR_BGN = value * (1/BGN_EUR);
    else if(c1 == "BGN" && c2 == "EUR")
        BGN_EUR = value * BGN_EUR;

    cout << result << endl;
    return 0;
}

int exec_rate()
{
    string c1, c2;
    cin >> c1 >> c2;

    double rate;
    if(c1 == "BGN" && c2 == "USD")
        rate = BGN_USD;
    else if(c1 == "USD"&& c2 == "BGN")
         rate = 1/BGN_USD;
    else if(c1 == "USD" && c2 == "EUR")
        rate = USD_EUR;
    else if(c1 == "EUR" && c2 == "USD")
        EUR_USD = 1/USD_EUR;
    else if(c1 == "EUR" && c2 == "BGN")
        EUR_BGN = 1/BGN_EUR;
    else if(c1 == "BGN" && c2 == "EUR")
        BGN_EUR = BGN_EUR;

    cout << rate << endl;
    return 0;
}

int exec_exit()
{
    cout << "Goodbye." << endl;
    return EXIT;
}

int execute_command(const string &cmd)
{
    if(cmd == "set")
        return exec_set();
    else if(cmd == "convert")
        return exec_convert();
    else if(cmd == "rate")
        return exec_rate();
    else if(cmd == "exit")
        return exec_exit();
    else
        cout << "Unrecognized command." << endl;
    return 0;
}

int main()
{
    string cmd;
    while(cin >> cmd)
    {
        int resultCode = execute_command(cmd);
        if(resultCode == EXIT)
            break;
    }
    return 0;
}

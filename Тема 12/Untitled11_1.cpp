#include <iostream>
using namespace std;

string name;

int execute_set()
{
    cin>> name;
    return 0;
}

int execute_exit()
{
    cout<< "Goodbye, " << name << "." << endl;
    return -1;
}

int execute_hello()
{
    cout<< "Hello, " << name << "!" << endl;
    return 0;
}

int execute(const string &cmd)
{
    if(cmd == "set")
        return execute_set();
    else if(cmd == "hello")
        return execute_hello();
    else if(cmd == "exit")
        return execute_exit();
    else
        cout<< "Unrecognized command." << endl;

}

int main()
{
    string cmd;
    while(cin>> cmd)
    {
        int resultCode = execute(cmd);
        if(resultCode == -1)
            break;
    }

    return 0;
}

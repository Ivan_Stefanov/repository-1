#include <iostream>
using namespace std;

int main()
{
    string name;
    string cmd;
    while(cin>> cmd)
    {
        if(cmd == "set")
        {
            cin>> name;
        }
        else if(cmd == "hello")
        {
            cout<< "Hello, " << name << "!" << endl;
        }
        else if(cmd == "exit")
        {
            cout<< "Goodbye, " << name << "." << endl;
            break;
        }
        else
            cout<< "Unrecognized command." << endl;
    }

    return 0;
}

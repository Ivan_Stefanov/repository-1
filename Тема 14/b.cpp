#include <iostream>
#include <string>
#include <ctype.h>

using namespace std;

int digit_value(char digit)
{
    return digit - '0';
}
char rotate(char c, int r)
{
    char base = isupper(c) ? 'A' : 'a';
    int n = c-base;
    n = (n+r)%26;
    return base + n;
}

void encrypt(string &s, const string &key, int &keyOffset)
{
    for(int i=0; i<s.length(); i++)
    {
        if(isalpha(s[i]))
        {
            int rot = digit_value(key[keyOffset]);
            s[i] = rotate(s[i], rot);

            keyOffset = (keyOffset+1) % key.length();
        }
    }
}

int main()
{
    string key;
    int idx = 0;
    cin >> key;

    string s;
    while(getline(cin, s))
    {
        encrypt(s, key, idx);
        cout << s << endl;
    }
    return 0;
}

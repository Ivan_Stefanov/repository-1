/***
FN:F79374
PID:1
GID:1
*/

#include <iostream>

using namespace std;

bool isPrime (int n)
{
    if (n == 2)
        return true;

    else if (n % 2 == 0)
        return false;

   for (int i=3; i*i<=n; i+=2)
   {
       if (n % i == 0)
        return false;
   }
   return true;
}

int main()
{
	int n;

	while (cin>> n)
	{
	    if (n < 2)
            cout<< "N/A" << endl;

	    if (isPrime(n))
            cout<< "YES" << endl;

        else
            cout<< "NO" << endl;
	}

	return 0;
}

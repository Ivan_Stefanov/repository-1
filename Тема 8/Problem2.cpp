/***
FN:F79374
PID:2
GID:1
*/

#include <iostream>

using namespace std;

void do_the_thing(const string &str)
{
    for (int i=str.length()-1; i>=0; i--)
    {
        cout<< str[i] << '|';
    }
    cout<< endl;
}

int main()
{
	string input;

	while (cin>> input)
    {
        if (input == "exit")
            break;

        do_the_thing(input);
    }

	return 0;
}

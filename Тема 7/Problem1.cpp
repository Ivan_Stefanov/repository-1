/***
FN:F79374
PID:1
GID:1
*/

#include <iostream>

using namespace std;

//functions here
double add(double a, double b)
{
    return a+b;
}

double substract(double a, double b)
{
    return a-b;
}

double mult(double a, double b)
{
    return a*b;
}

double divide(double a, double b)
{
    return a/b;
}

double do_the_math(double a, double b, char op)
{
    if(op == '+')
    {
        return add(a, b);
    }

    else if(op == '-')
    {
        return substract(a, b);
    }

    else if(op == '*')
    {
        return mult(a, b);
    }

    else if(op == '/')
    {
        return divide(a, b);
    }

    return -1;
}

int main()
{
	double lo, ro;
	char op;
	cin>> lo >> ro >> op;
	double result = do_the_math(lo, ro, op);
	cout<< result << endl;
	return 0;
}

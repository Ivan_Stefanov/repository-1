#include <iostream>
#include <vector>

using namespace std;

void executeSum(const vector<double> &v)
{
    double sum=0;
    for(int i=0; i<v.size(); i++)
    {
        sum+= v[i];
    }

    cout << sum << endl;
}

void executeAvg(const vector<double> &v)
{
    double sum=0;
    for(int i=0; i<v.size(); i++)
    {
        sum+= v[i];
    }

    cout << sum / v.size() << endl;
}

void executeMin(const vector<double> &v)
{
    double minValue = v[0];
    for(int i=1; i<v.size(); i++)
    {
        minValue = min(minValue, v[i]);
//        if(v[i] < minValue)
  //          minValue = v[i];

    }
    cout << minValue << endl;
}

void executeMax(const vector<double> &v)
{
    double maxValue = v[0];
    for(int i=1; i<v.size(); i++)
    {
        maxValue = max(maxValue, v[i]);
//        if(v[i] > maxValue)
  //          maxValue = v[i];

    }
    cout << maxValue << endl;
}

void executeIns(vector<double> &v)
{
    double next;
    cin >> next;
    v.push_back(next);
}

void executeDel(vector<double> &v)
{
    int idx;
    cin >> idx; //indexed from 1
    idx = idx-1; //convert to 0-based index
    v.erase(v.begin()+idx);
}

void prn(const vector<double> &v)
{
    for(int i=0; i<v.size(); i++)
    {
        cout << v[i] << ", ";
    }
    cout << endl;
}

int main()
{
    vector<double> numbers;
    int n;
    cin >> n;
    numbers.resize(n);
    for(int i=0; i<n; i++)
    {
        cin >> numbers[i];
    }

    string cmd;
    while(cin >> cmd)
    {
        if(cmd == "sum")
        {
            executeSum(numbers);
        }
        else if(cmd == "avg")
        {
            executeAvg(numbers);
        }
        else if(cmd == "max")
        {
            executeMax(numbers);
        }
        else if(cmd == "min")
        {
            executeMin(numbers);
        }
        else if(cmd == "ins")
        {
            executeIns(numbers);
        }
        else if(cmd == "del")
        {
            executeDel(numbers);
        }
    }


    return 0;
}

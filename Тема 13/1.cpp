#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void prn(const vector<double> &v)
{
    for(int i=0; i<v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

double sum(const vector<double> &v)
{
    double result = 0;
    for(int i=0; i<v.size(); i++)
    {
        result += v[i];
    }
    return result;
}

void exec_sum(const vector<double> &v)
{
    cout << sum(v) << endl;
}
void exec_avg(const vector<double> &v)
{
    cout << sum(v) / v.size() << endl;
}

void exec_min(const vector<double> &v)
{
    double best = v[0];
    for(int i=1; i<v.size(); i++)
    {
        best = min(v[i], best);
    }
    cout << best << endl;
}

void exec_max(const vector<double> &v)
{
    double best = v[0];
    for(int i=1; i<v.size(); i++)
    {
        best = max(v[i], best);
    }
    cout << best << endl;
}

void exec_ins(vector<double> &v)
{
    double value;
    cin >> value;
    v.push_back(value);
}

void exec_del(vector<double> &v)
{
    int pos;
    cin >> pos;
    int index = pos-1;
    v.erase(v.begin() + index);
}

int main()
{
    int n;
    cin >> n;
    vector<double> v(n);
    for(int i=0; i<n; i++)
        cin >> v[i];

    string cmd;
    while(cin >> cmd)
    {
        if(cmd == "sum")
            exec_sum(v);
        else if(cmd == "avg")
            exec_avg(v);
        else if(cmd == "min")
            exec_min(v);
        else if(cmd == "max")
            exec_max(v);
        else if(cmd == "ins")
            exec_ins(v);
        else if(cmd == "del")
            exec_del(v);
        else
            cout << "Unrecognized command '" << cmd << "'!" << endl;
    }

    return 0;
}
